module SunsetCalculator exposing (calculateSunet,calculateSunrise) 

normalize : Float -> Float -> Float
normalize x base = if x < 0 then x + base 
                   else if (x>base) then x - base else x

cosZenith = -0.01454


-- Mode: Sunset - true, Sunrise - false
calculateTime : Bool -> Float -> Float -> Int -> Float 
calculateTime mode lat long dayOfYear =
  let longHour = long / 15
  in let
      t = toFloat dayOfYear + ((18.0 - longHour) / 24.0)
      in let m = (0.9856 * t) - 3.289 
      in let l = normalize (m + (1.916 * sin(m*pi/180)) + (0.020 * sin(2 * m*pi/180)) + 282.634) 360
      in let ras = atan(0.91764 * tan(l * pi/180))*180/pi
      in  let lquadrant = toFloat (floor(l/90)) * 90
              raquadrant = toFloat (floor(ras/90)) * 90
              ra = ras + (lquadrant - raquadrant)
              in let sinDec = 0.39782 * sin(l*pi/180)
                     cosDec = cos(asin(sinDec)) 
                     in let cosH = (cosZenith - (sinDec * sin(lat*pi/180))) / (cosDec * cos(lat*pi/180))
                     in let h = case mode of True -> (acos(cosH)*180/pi)/15
                                             False -> (360 - (acos(cosH)*180/pi))/15
                     in let t2= h + ra/15 - (0.06571 * t) - 6.622
                     in normalize (t2 - longHour) 24

calculateSunet : Float -> Float -> Int -> Float 
calculateSunet lat long dayOfYear = calculateTime True lat long dayOfYear  

calculateSunrise : Float -> Float -> Int -> Float 
calculateSunrise lat long dayOfYear = calculateTime False lat long dayOfYear