module Dictionary exposing (..)

import Time exposing (..)
import Calendar exposing (..)

type Language = Pl | Eng

type Label = Sunset 
            | Sunrise 
            | SummerTime 
            | WinterTime
            | SelectMunicipality

english : Label -> String 
english label = case label of Sunset -> "Sunset"
                              Sunrise -> "Sunrise"
                              SummerTime -> "Summer time [UTC+2]"
                              WinterTime -> "Winter time [UTC+1]" 
                              SelectMunicipality -> "Select a municipality"

polish : Label -> String 
polish label = case label of  Sunset -> "Zachód"
                              Sunrise -> "Wschód"
                              SummerTime -> "Czas letni [UTC+2]"
                              WinterTime -> "Czas zimowy [UTC+1]"
                              SelectMunicipality -> "Wybierz gminę"

getDictionary : Language -> Label -> String
getDictionary language = case language of Pl -> polish
                                          Eng -> english       

printMonthEng : Month -> String
printMonthEng month = case month of 
 Jan -> "January"
 Feb -> "Febuary"
 Mar -> "March"
 Apr -> "April"
 May -> "May"
 Jun -> "June"
 Jul -> "July"
 Aug -> "August"
 Sep -> "September"
 Oct -> "October"
 Nov -> "November"
 Dec -> "December"   

printMonthPl : Month -> String
printMonthPl month = case month of 
 Jan -> "Stycznia"
 Feb -> "Lutego"
 Mar -> "Marca"
 Apr -> "Kwietnia"
 May -> "Maja"
 Jun -> "Czerwca"
 Jul -> "Lipca"
 Aug -> "Sierpnia"
 Sep -> "Września"
 Oct -> "Października"
 Nov -> "Listopada"
 Dec -> "Grudnia"    

printDateEng : Calendar.Date -> String
printDateEng date = let dayOfMonth = (String.fromInt <| Calendar.getDay date)
                        suffix = ordinal <| Calendar.getDay date 
                 in dayOfMonth ++ suffix ++" " ++ (printMonthEng <| Calendar.getMonth date)

printDatePl : Calendar.Date -> String
printDatePl date = let dayOfMonth = (String.fromInt <| Calendar.getDay date)
                   in dayOfMonth ++" " ++ (printMonthPl <| Calendar.getMonth date)

printDate : Language -> Calendar.Date -> String
printDate language = case language of Pl -> printDatePl
                                      Eng -> printDateEng  

ordinal : Int -> String 
ordinal number = let ordinal2 number2 = case (modBy 10 number2) of 1 -> "st"
                                                                   2 -> "nd"
                                                                   3 -> "rd"
                                                                   _ -> "th"  
                 in if (10 < number && number < 20) then "th" else ordinal2 number                                                            