import Browser
import Html exposing (Html, button, div, text, input,select,option)
import Html.Events exposing (onClick)
import Html.Attributes as H exposing (..)
import Html.Events exposing (onInput)
import Calendar
import Time exposing (..)
import Data exposing (Poland,County,dataSource,CountyState)
import Svg exposing(Svg,svg)
import Svg.Events exposing (..)
import Svg.Attributes as SA
import Color
import Random 
import MapPalettes
import SunsetCalculator
import Json.Decode as Json
import Dictionary exposing (..)

main = Browser.element { init = init, update = update, view = view, subscriptions = subscriptions}

-- MODEL

type Mode = Sunset | Sunrise
type Timezone = SummerTime | WinterTime | WithSaving

type alias Model = {
    day : Int,
    poland : Data.Poland,
    mode : Mode,
    timezone : Timezone,
    language : Language 
        }



init : () -> (Model, Cmd Msg)
init _ = ({day = 365//2, mode = Sunset, 
  timezone = SummerTime, 
  language = Eng,
  poland = (paintPoland Data.dataSource (updateCounty 182 Sunset SummerTime))}, 
  Cmd.none)

-- UPDATE
type Msg =  UpdateDay String 
          | Repaint 
          | SetSunset Bool 
          | SetSunrise Bool 
          | SetTimezone String
          | EnterCounty County
          | LeaveCounty County
          | LockCounty County
          | ChangeLanguage String

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of UpdateDay day -> (case String.toInt day of Just x ->  ({model | day = x} |> update Repaint )
                                                         Nothing -> (model, Cmd.none))
              Repaint  -> ({model | poland = (paintPoland model.poland (updateCounty model.day model.mode model.timezone))}, Cmd.none)
              SetSunset _ -> ({model | mode = boolToMode True} |> update Repaint)
              SetSunrise _ -> ({model | mode = boolToMode False} |> update Repaint)
              SetTimezone timezone -> let newTimezone = Maybe.withDefault model.timezone (parseTimezone timezone) 
                                      in ({model | timezone = newTimezone} |> update Repaint)
              EnterCounty county -> ({model | poland = markSelected county model.poland}, Cmd.none)
              LeaveCounty county -> ({model | poland = leave county model.poland}, Cmd.none)
              LockCounty  county -> ({model | poland = markLocked county model.poland}, Cmd.none)
              ChangeLanguage language -> ({model | language = parseLanguage language |> Maybe.withDefault model.language},Cmd.none) 
  

posixEpochDate : Calendar.Date
posixEpochDate = Calendar.fromPosix (Time.millisToPosix 0)

modeToBool : Mode -> Bool
modeToBool mode = case mode of 
  Sunset -> True
  Sunrise ->  False

boolToMode : Bool -> Mode
boolToMode mode = case mode of 
  True -> Sunset
  False ->  Sunrise

addDays : Calendar.Date -> Int -> Calendar.Date
addDays date n =
  case n of  
  0 -> date
  _ -> addDays (Calendar.incrementDay date) (n-1)

                     
printTime : Float -> String 
printTime time = 
                let hour = (String.fromInt <| floor time)
                in let minutesF = (floor(60*(time - toFloat(floor time))))
                in let minutes = if minutesF == 0 then "00" else 
                                    if minutesF < 10 then "0" ++ String.fromInt minutesF else
                                       String.fromInt minutesF
                in hour ++ ":" ++ minutes
                

updateColors : (List String) -> Data.Poland -> Data.Poland
updateColors colors poland = List.map2 updateColor poland colors


updateCounty : Int -> Mode -> Timezone -> Data.County ->  Data.County 
updateCounty day mode timezone county = let time = (calculateMethod mode) county.lat county.lon day 
                                                   |> (adjustTime timezone day)
                                            palette = getPalette mode
                          in {county | color = palette time, label = county.name ++ " " ++ printTime time}

calculateMethod : Mode -> (Float -> Float -> Int -> Float)
calculateMethod mode = case mode of Sunset -> SunsetCalculator.calculateSunet
                                    Sunrise -> SunsetCalculator.calculateSunrise

paintPoland : Data.Poland -> (Data.County -> Data.County) -> Data.Poland
paintPoland poland mapper = List.map mapper poland


getPalette : Mode -> (Float -> String)
getPalette mode = case mode of Sunset -> MapPalettes.sunsetColors
                               Sunrise -> MapPalettes.sunriseColors

adjustTime : Timezone -> Int -> Float -> Float
adjustTime timezone day time = case timezone of SummerTime -> time + 2
                                                WinterTime -> time + 1
                                                WithSaving -> time + (savingAdjustment day)

savingAdjustment : Int -> Float
savingAdjustment day = if (day < 90 || day > 299) then 1 else 2

parseTimezone : String -> Maybe Timezone
parseTimezone msg = case msg of "winter" -> Just WinterTime
                                "summer" -> Just SummerTime
                                "withsaving" -> Just WithSaving
                                _ -> Nothing


parseLanguage : String -> Maybe Language
parseLanguage msg = case msg of "pl" -> Just Pl
                                "eng" -> Just Eng 
                                _ -> Nothing                               

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

-- VIEW
drawCounty : County -> Html Msg
drawCounty county = svg[]
    [Svg.path  
    [SA.d county.borders, SA.id county.id, SA.fill county.color, strokeWidth county.state, strokeColor county.state,
      Html.Events.onMouseEnter (EnterCounty county),
      Html.Events.onMouseLeave (LeaveCounty county),
      Html.Events.onClick (LockCounty county)
      ] 
    [Svg.title [][text county.label]]]

updateColor : County -> String -> County
updateColor county color = {county | color = color}

markSelected : County -> Data.Poland  -> Data.Poland 
markSelected county poland = if county.state == Data.Locked then poland else
                             let notSelected = List.filter (notThisCounty county) poland |> List.map unselectCounty
                                 selected = {county | state = Data.Selected} 
                             in selected :: notSelected

unselectCounty : County -> County
unselectCounty c =  {c | state = if(c.state == Data.Selected) then Data.None else c.state}

markLocked : County -> Data.Poland  -> Data.Poland 
markLocked county poland = let notLocked = List.filter (notThisCounty county) poland |> List.map(\ c -> {c | state = Data.None})
                               locked = {county | state = Data.Locked} in locked :: notLocked

leave : County -> Data.Poland  -> Data.Poland 
leave county poland = if county.state == Data.Locked then poland else
                      let rest = List.filter (notThisCounty county) poland 
                          justVisited = {county | state = Data.None} in justVisited :: rest

notThisCounty : County -> County -> Bool
notThisCounty this that = this.id /= that.id

strokeWidth : CountyState -> Html.Attribute msg
strokeWidth selected = case selected of Data.Selected -> SA.strokeWidth "1px"
                                        Data.Locked -> SA.strokeWidth "3px"
                                        _ -> SA.strokeWidth "1px"

strokeColor : CountyState -> Html.Attribute msg
strokeColor selected = case selected of Data.Selected -> SA.stroke "#2C2C2C"
                                        Data.Locked -> SA.stroke "#2C2C2C"
                                        _ -> SA.strokeWidth ""                                         

drawPoland : Poland -> Html Msg 
drawPoland poland  = svg [SA.style "position:relative", SA.viewBox "0 0 800 744", SA.width "95%", SA.height "95%" , title "Loading..."]
                         (List.map drawCounty poland)

mapaSVG = drawPoland Data.dataSource

getLabel : Poland -> Language -> String
getLabel model language = let selected = (List.filter (\ c -> c.state == Data.Selected)) model |> List.head
                              locked = (List.filter (\ c -> c.state == Data.Locked)) model |> List.head 
                          in Maybe.map (\c -> c.label) selected 
                          |> Maybe.withDefault (Maybe.map (\c -> c.label) locked |> Maybe.withDefault (Dictionary.getDictionary language Dictionary.SelectMunicipality))
          

printLabel : Model -> Html msg
printLabel model =text <| (Dictionary.printDate model.language <| addDays posixEpochDate model.day) ++ " - " ++ getLabel model.poland model.language    

view : Model -> Html Msg
view model =
  div [style "display" "contents"] [
         div[style "display" "flex"][
        Html.a[H.style "cursor" "pointer" , Html.Events.onClick (ChangeLanguage "pl")][text "PL"],
        text " | ",
        Html.a[H.style "cursor" "pointer" ,Html.Events.onClick (ChangeLanguage "eng")][text "EN"]

      ],
    div[][drawPoland model.poland],
        input [ type_ "range" , H.style "width" "inherit" , H.min "0" , H.max "364", value  <| String.fromInt model.day, onInput UpdateDay][],
    div[][printLabel model],
    div[style "display" "flex"][
    div[style "display" "flex"][
      div[][input[type_ "radio", style "margin" "4px", checked <| not <| modeToBool model.mode, Html.Events.onCheck SetSunrise][], text (Dictionary.getDictionary model.language Dictionary.Sunrise)],
      div[][input[type_ "radio", style "margin" "4px", checked  <| modeToBool model.mode, Html.Events.onCheck SetSunset][],text (Dictionary.getDictionary model.language Dictionary.Sunset)]
    ],
    div[style "display" "flex"][
    div[][
    select[onInput SetTimezone][
      option[value "winter"][text (Dictionary.getDictionary model.language Dictionary.WinterTime)],
      option[value "summer", selected True][text (Dictionary.getDictionary model.language Dictionary.SummerTime)]
    --  option[value "withsaving"][text "With daylight saving [UTC+1 / UTC+2]"]
      ]]]
      ]
    ]