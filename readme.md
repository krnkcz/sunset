##### tl;dr:
This is a simple data visualisation written in Elm, wrote as a weekend project.

##### Background:
A few months ago the European Parliament has voted to end the magical practice of “daylight saving time”. Poland is not big enough to really need more than one timezone, although outskirts can differ up to one hour between their sunsets and sunrises. I created a simple visualisation to present both options. We have to decide until April 2020. Central European Time fits better from a geographical point of view, but many of us seem to like summer time more thanks to brigher and longer evenings.

##### Demo:
[Here.](https://kornakiewi.cz/posts/daylight-saving/)